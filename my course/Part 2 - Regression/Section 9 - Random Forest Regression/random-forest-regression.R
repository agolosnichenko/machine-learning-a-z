## Random Forest Regression

## Importing Dataset
dataset = read.csv('Position_Salaries.csv')
dataset = dataset[,2:3]

## Taking care of missing data
#dataset$Age <- ifelse(is.na(dataset$Age), ave(dataset$Age, FUN = function(x) mean(x,na.rm = T)), dataset$Age)
#dataset$Salary <- ifelse(is.na(dataset$Salary), ave(dataset$Salary, FUN = function(x) mean(x,na.rm = T)), dataset$Salary)

## Encoding categorical variables
#dataset$Country = factor(dataset$Country, levels = c('France', 'Spain', 'Germany'), labels = c(1,2,3))
#dataset$Purchased = factor(dataset$Purchased, levels = c('No', 'Yes'), labels = c(0,1))

## Splitting the dataset into the Training Set and Test set
# library(caTools)
# set.seed(123)
# split = sample.split(dataset$Purchased, SplitRatio = 0.8)
# training_set = subset(dataset, split == T)
# test_set = subset(dataset, split == F)

## Feature Scaling
#training_set[,2:3] = scale(training_set[,2:3])
#test_set[,2:3] = scale(test_set[,2:3])

## Fitting Random Forest to the dataset
library(randomForest)
set.seed(1234)
regressor = randomForest(x = dataset[1],
                         y = dataset$Salary,
                         ntree = 500)


## Predicting a new result
y_pred = predict(regressor, newdata = data.frame(Level = 6.5))

## Visualising the Random Forest results (for higher resolution and smoother curve)
library(ggplot2)
x_grid = seq(min(dataset$Level), max(dataset$Level), 0.01)
ggplot() + 
  geom_point(aes(dataset$Level, dataset$Salary), 
             color = 'red') + 
  geom_line(aes(x_grid, predict(regressor, newdata = data.frame(Level = x_grid))), 
            color = 'blue') +
  ggtitle('Truth or Bluf (Random Forest)') + 
  xlab('Level') +
  ylab('Salary')


