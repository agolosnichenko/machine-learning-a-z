## Decision Tree Regression

## Importing the Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

## Importing the dataset
dataset =  pd.read_csv('Position_Salaries.csv')
X = dataset.iloc[:, 1:2].values
y = dataset.iloc[:, 2].values

"""
## Feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
"""

## Fitting Decision Tree to the dataset
from sklearn.tree import DecisionTreeRegressor
regressor = DecisionTreeRegressor(random_state=0)
regressor.fit(X,y)

## Predicting a new result
y_pred = regressor.predict(([[6.5]]))

## Visualising the Regression results (for higher resolution and smoother curve)
X_grid = np.arange(min(X),max(X), step = 0.01)
X_grid = X_grid.reshape(len(X_grid),1)
plt.scatter(X,y,color = 'red')
plt.plot(X_grid,regressor.predict(X_grid), color = 'blue')
plt.title('Truth or bluf? (Regression Model)');
plt.xlabel('Position')
plt.ylabel('Salary')
plt.show()
