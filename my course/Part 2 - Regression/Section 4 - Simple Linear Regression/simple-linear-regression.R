## Simple Linear Regression

## Importing Dataset
dataset = read.csv('Salary_Data.csv')
dataset = dataset[,]

## Splitting the dataset into the Training Set and Test set
library(caTools)
set.seed(123)
split = sample.split(dataset$Salary, SplitRatio = 2/3)
training_set = subset(dataset, split == T)
test_set = subset(dataset, split == F)

## Feature Scaling
#training_set[,2:3] = scale(training_set[,2:3])
#test_set[,2:3] = scale(test_set[,2:3])

## Fitting Simple Linear Regression to the Training set
regressor = lm(formula = Salary ~ YearsExperience, data = training_set)
summary(regressor)

## Predicting the Test Set result
y_pred = predict(regressor, newdata = test_set)

## Visualising the Training Set result
library(ggplot2)
ggplot() + geom_point(aes(training_set$YearsExperience, training_set$Salary),
                      colour = 'red') + 
            geom_line(aes(training_set$YearsExperience, predict(regressor, training_set)),
                      colour = 'blue') + 
            ggtitle('Salary vs. Experience (Training Set)') + 
            xlab('Years of Experience') + 
            ylab('Salary')

## Visualising the Test Set result
ggplot() + geom_point(aes(test_set$YearsExperience, test_set$Salary),
                      colour = 'red') + 
  geom_line(aes(training_set$YearsExperience, predict(regressor, training_set)),
            colour = 'blue') + 
  ggtitle('Salary vs. Experience (Test Set)') + 
  xlab('Years of Experience') + 
  ylab('Salary')
