## Artificial Neural Networks

## Data Preprocessing

## Importing the Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

## Importing the dataset
dataset =  pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values

"""
## Take care of missing data
from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis=0)
imputer = imputer.fit(X[:,1:3])
X[:,1:3] = imputer.transform(X[:,1:3])
"""

## Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X_1 = LabelEncoder()
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])
labelencoder_X_2 = LabelEncoder()
X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])
onehotencoder = OneHotEncoder(categorical_features=[1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:,1:]

## Splitting the dataset into the Training Set and Test set
from sklearn.model_selection import train_test_split 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)


## Feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

## Importing the Keras libraries and packages
import keras
from keras.models import Sequential
from keras.layers import Dense

## Initialising the ANN
classifier = Sequential()

## Adding the input layer and the first hidden layer
classifier.add(Dense(units = 6,
                     kernel_initializer = 'uniform',
                     activation = 'relu',
                     input_dim = 11
                     ))

## Adding the second hidden layer
classifier.add(Dense(units = 6,
                     kernel_initializer = 'uniform',
                     activation = 'relu'
                     ))

## Adding the output layer
classifier.add(Dense(units = 1,
                     kernel_initializer = 'uniform',
                     activation = 'sigmoid'
                     ))

## Compiling the ANN
classifier.compile(optimizer = 'adam', 
                   loss = 'binary_crossentropy',
                   metrics = ['accuracy'])

## Fitting the ANN to the training set
classifier.fit(x = X_train,
               y = y_train,
               batch_size = 10,
               epochs = 100)

## Predicting a new result
y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)