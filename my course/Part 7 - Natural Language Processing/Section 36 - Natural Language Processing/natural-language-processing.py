## Natural Language Processing

## Importing the Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

## Importing the dataset
dataset =  pd.read_csv("Restaurant_Reviews.tsv", 
                       delimiter = '\t',
                       quoting = 3)

## Cleaning the texts
import re
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
corpus = []
for i in range(0, 1000):
    review = re.sub(pattern = '[^a-zA-Z]', repl = ' ', string = dataset['Review'][i])
    review = review.lower()
    review = review.split()
    ps = PorterStemmer()
    review = [ps.stem(word) for word in review if not word in set(stopwords.words('english'))]
    review = ' '.join(review)
    corpus.append(review)
    
## Creating the Bag of Words Model
from sklearn.feature_extraction.text import CountVectorizer
cv = CountVectorizer(max_features = 1500)
X = cv.fit_transform(corpus).toarray()
y = dataset.iloc[:,1].values

## Splitting the dataset into the Training Set and Test set
from sklearn.model_selection import train_test_split 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

## Fitting Naive Bayes to the dataset
from sklearn.naive_bayes import GaussianNB
classifier = GaussianNB()
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

## Evaluating the model performance
accuracy = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision = cm[1][1] / (cm[1][1] + cm[0][1])
recall = cm[1][1] / (cm[1][1] + cm[1][0])
f1_score = 2 * precision * recall / (precision + recall)


############
##HOMEWORK##
############
cm_naive_bayes = cm
accuracy_naive_bayes = accuracy
precision_naive_bayes = precision
recall_naive_bayes = recall
f1_naive_bayes = f1_score

## Fitting Logistic Regression to the dataset
from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state=0)
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
cm_logistic = cm

## Evaluating the model performance
accuracy_logistic = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision_logistic = cm[1][1] / (cm[1][1] + cm[0][1])
recall_logistic = cm[1][1] / (cm[1][1] + cm[1][0])
f1_logistic = 2 * precision_logistic * recall_logistic / (precision_logistic + recall_logistic)

## Fitting KNN to the dataset
from sklearn.neighbors import KNeighborsClassifier
classifier = KNeighborsClassifier(n_neighbors=3, metric='minkowski', p=2)
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
cm_knn = cm

## Evaluating the model performance
accuracy_knn = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision_knn = cm[1][1] / (cm[1][1] + cm[0][1])
recall_knn = cm[1][1] / (cm[1][1] + cm[1][0])
f1_knn = 2 * precision_knn * recall_knn / (precision_knn + recall_knn)

## Fitting SVM to the dataset
from sklearn.svm import SVC
classifier = SVC(kernel = 'linear',
                 random_state = 0)
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
cm_svm = cm

## Evaluating the model performance
accuracy_svm = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision_svm = cm[1][1] / (cm[1][1] + cm[0][1])
recall_svm = cm[1][1] / (cm[1][1] + cm[1][0])
f1_svm = 2 * precision_svm * recall_svm / (precision_svm + recall_svm)

## Fitting Kernel SVM to the dataset
from sklearn.svm import SVC
classifier = SVC(kernel = 'rbf',
                 random_state = 0)
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
cm_kernel_svm = cm

## Evaluating the model performance
accuracy_kernel_svm = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision_kernel_svm = cm[1][1] / (cm[1][1] + cm[0][1])
recall_kernel_svm = cm[1][1] / (cm[1][1] + cm[1][0])
f1_kernel_svm = 2 * precision_kernel_svm * recall_kernel_svm / (precision_kernel_svm + recall_kernel_svm)

## Fitting Decision Tree to the dataset
from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion = 'entropy', random_state = 0)
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
cm_decision_tree = cm

## Evaluating the model performance
accuracy_decision_tree = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision_decision_tree = cm[1][1] / (cm[1][1] + cm[0][1])
recall_decision_tree = cm[1][1] / (cm[1][1] + cm[1][0])
f1_decision_tree = 2 * precision_decision_tree * recall_decision_tree / (precision_decision_tree + recall_decision_tree)

## Fitting Random Forest to the dataset
from sklearn.ensemble import RandomForestClassifier
classifier = RandomForestClassifier(n_estimators = 10,
                                    criterion = 'entropy',
                                    random_state = 0)
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
cm_random_forest = cm

## Evaluating the model performance
accuracy_random_forest = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision_random_forest = cm[1][1] / (cm[1][1] + cm[0][1])
recall_random_forest = cm[1][1] / (cm[1][1] + cm[1][0])
f1_random_forest = 2 * precision_random_forest * recall_random_forest / (precision_random_forest + recall_random_forest)

## Fitting CART to the dataset
from sklearn.ensemble import RandomForestClassifier
classifier = RandomForestClassifier(n_estimators = 10,
                                    criterion = 'gini',
                                    random_state = 0)
classifier.fit(X_train, y_train)

## Predicting a new result
y_pred = classifier.predict(X_test)

## Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
cm_cart = cm

## Evaluating the model performance
accuracy_cart = (cm[1][1] + cm[0][0]) / (cm[1][1] + cm[0][0] + cm[0][1] + cm[1][0])
precision_cart = cm[1][1] / (cm[1][1] + cm[0][1])
recall_cart = cm[1][1] / (cm[1][1] + cm[1][0])
f1_cart = 2 * precision_cart * recall_cart / (precision_cart + recall_cart)
